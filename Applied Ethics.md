# Applied Ethics

## Why

Current civilization is built on the shoulders of giants. The sheer amount of stupidity it is able to handle is nothing short of spectacular. Just as spectacular are the structures which are sustained in a perpetually moving world.

Every change is a risk as much as it is an opportunity. However the winds of change are blowing ever stronger. Some structures are excellent at adapting. If we fail to live up to the opportunity other structures will happily wipe everything off and work on rebuilding.

While evolution is better than revolution, there are factors which make the status quo unmaintainable. Unfortunately some carry the risks of making the world too unethical. While all risks are a result of technological progress, the riskiest advancements are not in the field of information technology but rather in biology.

## Catalysts

**1. Open Source** [It](https://en.wikipedia.org/wiki/Open_source) is the least risky technological development as it has been present for the longest. Linux becoming the most popular operating system in the world demonstrates [the bazaar's superiority over the cathedral](https://en.wikipedia.org/wiki/The_Cathedral_and_the_Bazaar). First it is operating systems and protocols, then social networks, and then everything else.

**2. Decentralized systems** It is a subset of Open source software. Distributed nature of the decentralized systems makes them in effect unbannable. Torrents are a good example of decentralized file sharing. They only became stealthier over time.

A more recent development is the decentralization of trust. Moving trust from the authority and placing it in the protocol. It is a much riskier development because it was immediately applied to finances. The disinflationary nature of [Bitcoin](https://en.wikipedia.org/wiki/Bitcoin) will be painful to the debt based economy. Once the flow of rewards diminishes enough Bitcoin will de facto become deflationary. In effect it is a time bomb. Tick tock - new block. Fighting it directly could win more time at most, but I am not a fan of burning books nor people.

Smart contracts are the least explored territory but no less powerful. Decentralized ids will supercharge possibilities offered by smart contracts. 

**3. Artificial Intelligence (AI)** Research in the field of AI has existed for decades, but only recent public advancements marked by a widespread availability of [ChatGPT](https://en.wikipedia.org/wiki/ChatGPT) made everyone pay attention. Funny how 'Open' can sometimes get co-opted.

There will be truly open source [alternatives](https://en.wikipedia.org/wiki/George_Hotz), perhaps even decentralized. Models will get better, world will get weirder. AI is undoubtedly a wild card, but I'm feeling lucky.

**4. Life Extension Technology (LET)** One's heart doesn't just stop beating because [telomeres](https://en.wikipedia.org/wiki/Telomere) get too short. Aging brings diseases. The whole healthcare system is preoccupied with curing diseases. That is a LET in some sense. However [extreme](https://en.wikipedia.org/wiki/David_A._Sinclair) slowing down of aging is in the cards, making it the riskiest of technological advancements.

There would be plenty of people who would use LET without giving it a second thought. At the very least a one should realize that unaging body will require committing suicide if existence ever becomes too boring.

Furthermore an unethical being who does not age has the potential of making the world infinitely worse off.

A good start for a person with an interest in using LET is to refrain from killing or consuming [megafauna](https://en.wikipedia.org/wiki/Megafauna). Then progress towards vegetarianism and veganism. At some point after LET is available another technology will be developed allowing to recycle matter into delicious and nutritious food. Using LET without it will require to justify one's existence.

Society will always be ruled by those willing to kill. To kill an unethical being who uses LET is not only ethical but a necessary measure. Indeed rulers who ensure limited lifespans of unethical beings are wise kings. In contrast a ruler who is unable to limit access to LET nor to control the lifespans of unethical beings, is incompetent and cannot justify one's position.

Advancements in LET imply drastically reducing the system's tolerance for stupidity.

Weighing possible risks makes one appreciate the status quo. However since status quo is hardly an option it is wise to actively adopt these technologies and make for a smoother transition.

**Personal reasons**

I want to be able to leave my car unlocked. Maybe this can be achieved by surveillance and harsh punishment, but may also be an outcome of an ethical culture.

When I see a homeless I want to know that one is in this situation by choice and not by happenstance. Of course if someone desires to live like a bum one should be free to do so.

Long and interesting existence.

## How

The system is more prosperous if beings within it are happier.

The system is more ethical if it is not based on violence. Violence always remains a measure but society can be structured around voluntarism.

To achieve such a system we can use human desire to move up the status hierarchy. The more value an individual brings, the higher status one is assigned. It is similar to a social credit score. The key difference is that we start as free equal individuals. We determine what is valuable for us and sacrifice a bit of equality by imposing status hierarchy to incentivize generating value.

If someone is generating orders of magnitude more value than the other individuals it seems rational that one should be able to skip the queue at the store or get the green at the traffic lights. If someone is generating so much value that we are all able to live in a skyscraper it seems fair that this individual gets to choose the apartment floor first.

Individuals contributing the most would have more say in how those resources are allocated. Status hierarchy is determined by the voting coefficients (VCs).

One-dollar-one-vote system is not desirable as it gives too much power to the extremely wealthy individuals. Logarithmic function serves as a much better way to calculate a VC.

VC = 1 + ln(1 + Contribution/Median contribution)

This way every member has at least a voting power of 1. In the beginning additional contribution raises VC more sharply, but the incremental impact for large contributions tapers off.

In order to have a vote power of 2, one would have to contribute approximately 1.7 times more than the average member. Voting power of 5 requires a 55x larger contribution, but to have a voting power of 10 one would have to contribute 8000x the median contribution.

In order to reduce opportunities of gaming the system by momentarily inflating voting power it is possible to use [moving averages](https://en.wikipedia.org/wiki/Moving_average) of VCs. They are superior to calculating contribution totals, because contributions during periods of reduced budgets are that much more valuable.

Another way to inflate voting power would be to collude with friends and instead of making one large contribution split it among as many members as possible. This is an acceptable risk. In fact contributing in someone else's name could even be baked into the protocol. Boosting someone's VC as a form of flattery or gratitude.

Furthermore, dropping VCs for constitutional decision voting is an option. After all it is just a game and we all remain equal. And of course everyone is free to leave at any time.

This can be coded as a smart contract. For a large-scale adoption a decentralized id verification would be desirable. However for a proof of concept a workaround is possible - making some address or a [public key](https://en.wikipedia.org/wiki/Public-key_cryptography) a group member if it is vetted by sufficient percentage of the existing members.

Groups with love and trust bonds between members are likely to function even with faulty voting infrastructure parameters. However more important are the implications of facilitating functional group behaviors without immediate emotional ties and scaling beyond [150](https://en.wikipedia.org/wiki/Dunbar%27s_number).

There can be multiple experiments run at the same time. We can learn from our mistakes and successes. There are risks. There are also risks of not trying. If we succeed we end up with a better system. Be it more prosperous or more ethical. We will still be able to have the same stories.


Source code for Applied Ethics app demonstrating contribution based voting algorithm can be found [here](https://gitlab.com/tadas.tabulevicius/applied-ethics-app). Install Android app from the [Play store](https://play.google.com/store/apps/details?id=lt.appliedethics.app).

----
Whoever wishes to exist as equal among equals - deserves it.
