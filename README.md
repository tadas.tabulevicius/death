# Death

We live in turbulent times. Most of the western world is in lockdown since 2020-03-16 due to pandemic.
The inherent instability of our world system stems from our inability to be honest. Foremost with ourselves. With honesty comes compassion and forgiveness.

Let's look honestly at our current world, so we can imagine something different - a post scarcity world, granting long and interesting existence.

Current monetary system is set up so that money is worth less over time. It ensures that people will spend it. There is demand for products. People are employed.

Money is created through lending. People do not lose trust in the currency, because it is needed to cover debts.

Unless new loans are issued at ever increasing pace there must be an economic crash. Giving back loans and interest effectively destroys money. Resulting deflation prevents people from spending, businesses go bankrupt, unemployment increases. Bankruptcies cancel debt. Previously profitable companies are acquired at a discount by those who saved during the boom, but mostly using newly issued bank loans. 

Having multiple countries puts pressure on a central bank. Lower inflation compared to the other countries will make the currency appreciate. It reduces exports, leading to similar effects as deflation. Having inflation is not enough, it must be comparable to the competing countries.

In today's world capital flows relatively easily, a full blown crash would also allow competing countries to strengthen their ownership in the economy. Central bank has to intervene soon after the panic starts, but before the capital moves in from a competing country.


**The Empire**

The US became a dominant world power after WWII. Dollar was under gold standard until [1971](https://en.wikipedia.org/wiki/Gold_standard#Bretton_Woods). It remained the dominant world currency, because oil was priced in dollars.

The agreements were reached between the US and countries like Saudi Arabia or China. The US would import from them, but they would buy [US bonds](https://www.bloomberg.com/news/features/2016-05-30/the-untold-story-behind-saudi-arabia-s-41-year-u-s-debt-secret).

The threat of selling bonds cannot be underestimated. It was precisely used as a leverage by US to force British empire withdrawal from [Suez in 1956](https://en.wikipedia.org/wiki/British_Empire#Suez_and_its_aftermath). Selling large quantities of bonds has a power to destabilize the currency.

IMF used to advise countries like South Korea in [financial crises](https://en.wikipedia.org/wiki/1997_Asian_financial_crisis#IMF_role) to apply measures of belt tightening and allowing bankruptcies. However when crises hit developed countries, the approach of bailouts and stimulus is used to overturn it. The US was first to use this approach in the Great Recession and recovered faster. Europe lagged behind until the same approach was adopted. It is usually following the "whatever it takes'' statement by the central banker.

US tries to prevent a full blown crash, because it does not want the competing countries acquiring a significant part of its economy. In addition the US pension schemes are also affected by the market. Every crisis requires an ever greater rescue package to bail out the economy, because without the bankruptcies the total debt increases. Interest rates are lowered as part of the stimulus package.

As long as a company has higher returns than the interest rate it has incentive to borrow money and repurchase its shares. Tying CEO bonuses to the share price increase is another incentive for share buybacks. Companies become very vulnerable to economic downturns due financial leveraging through share buybacks.

FED provides liquidity to the markets by buying assets such as bonds on the secondary market. Effectively money is created out of thin air. It stimulates the economy and devalues the currency. There are two reasons why it does not make competing countries happy:
1.  Lost competitiveness. Due to diminished dollar value
2.  Countries, which have US bonds, effectively see their savings reduced

If the US creates 6 trln dollars for economic stimulus, China loses political leverage over 1 trln held in US bonds. Other countries as well. It will be easier for the US to repurchase in case China dumps their bond holdings.

In theory the US could freeze China's accounts. That would sow distrust among other nations so it would be a measure of last resort.


**The Society**

Intellect is the most important factor determining an individual's ability to advance in societal hierarchy. The ability to recognize patterns and think in abstractions will eventually lead to smart people taking over the system. A society that is able to direct its smartest people into the positions responsible for resource allocation will prosper.

During times of chaos the rule of the strong is dominant. Those are the worst times for society accompanied by high crime rates and corruption. As soon as the order starts to emerge, smart people will get to the top and usher in a new wave of prosperity. If at any point the system corrupts too much - smart people will find ways to subvert it.

Hierarchies are imposed by nature. In the mammalian world males compete for status. Females do not choose a partner, they mate with a high status male. Mammals by and large are not monogamous. [Male jealousy](https://en.wikipedia.org/wiki/Jealousy#Sexual_jealousy) is a mechanism used to ensure gene propagation.

Modern human society tries to impose monogamy, because high status males would choose multiple females and due to jealousy could prevent other males from mating. Leaving some males without a chance of procreation puts them in a "nothing to lose" position and quickly leads to social unrest.

Going against nature has its price. Divorce rates are about 50%. No one divorces because they are happy. Tragic seems the number if we account for couples who are unhappy but choose not to divorce.

Using money as a metric for status within social hierarchy is meant to direct competitiveness towards some socially valuable activity. It may be that 10% of the population will have a broad range of issues, and some will try to procure wealth in other ways. This may be acceptable as long as it does not greatly oppose the majority's view of fairness.

People tend to give preference to the well being of their own offspring as opposed to others. This introduces social classes and gradually corrupts the hierarchy. Once it is no longer based on competence and merit - catastrophe is just around the corner.

One's happiness has direct correlation with the happiness of others. Only an ignorant person can be a happy king in a suffering kingdom. Not everyone can live on the top floor, but everyone can drive a Tesla.

**Tragedy and Hope**

Some smart ones decided that our current system became too corrupt during the first years of Obama. Now it is only a matter of time. The system may have already been inherently unstable, but it is now a mere game of chicken. We may not have years anymore, but acting willingly we may minimize the descent into chaos.

Extremely smart people are good at self restraint and less likely to overindulge. By explicitly assigning them to allocate resources, we can ensure:
1.  Future intelligence of society
2.  Everyone has equal opportunity to grow
3.  Constant hierarchy recycling to represent competence
4.  Solution to the [tagedy of the commons](https://en.wikipedia.org/wiki/Tragedy_of_the_commons)
5.  Whatever else we desire.

Even if they cannot find a solution they will create that who will. One way or another humans will have to overcome a part of their nature. When we are no longer hostages of aging, Death will become a friend.

**2020-07-15 War**

War may serve different purposes from societal restructuring to economic reboot and resource capturing.However it often coincides with technological revolutions. Every side is convinced of its military advantage. Counting on a quick victory which will expand its sphere of influence. Unfortunately shooting conflicts are never quick to end.

Two major competing powers - the US and China have had rising tensions since the aftermath of the last financial crisis. It is unlikely that an all out war will be waged between these two countries as it would pose a risk to the planet.

Wars always have an economic element. In the current situation for the conflict to escalate into a proxy war, China would have to start rapid selling of its 1 trillion the US bond reserves. If the US froze Chinese accounts, that would be a substantial reason for China to retaliate by invading some disputed territory with resolve to defend against the US forces.

People are reluctant to kill other people for no good reason. During the first world war soldiers were purposefully missing when shooting at the enemy. That is why the enemy has to be dehumanized. Labeled with a nickname. Will 'gook' be brought back or something new will be created for Chinese?

General population is against war. Propaganda is used to stir the public towards it. Often combining it with the false flag operation. China of course cares less about what its population thinks. The US on the other hand does, but general sentiment is too sceptical of the mass media to be convincing. But the largest factor is that too many people feel disenfranchised. A man must have enough to risk his life defending it or have a vision worth fighting for.

For now tensions will continue to rise, as the US is about to get hit by the second wave of pandemic and continue monetary stimulus in order to avoid stock market meltdown. Both sides have capabilities to switch off the opponent's lights, but will refrain from it unless war escalates or succumbs to the "we must strike first" mentality of the generals.

**2020-07-30 Human loops**

Physical and mental states in humans are closely linked. Brain even maps certain feelings to particular parts of the body. Love is often experienced in the chest area, whereas disgust in the neck and stomach. It is common knowledge that stress causes high blood pressure. The less known fact is that high blood pressure and elevated heart rate will cause agitated mental state. This may be the reason why prolonged stress results in depression.

Heart is not the only organ affected by stress. Other muscles tense up as well. Neck, shoulders, back, even jaws. Tense muscles affect blood flow. Some people claim that they get headaches without their morning coffee. There is not enough blood reaching the brain. Chances are that such a person suffers from high blood pressure while the central nervous system tries to increase the heart rate. Now apparent is the vicious loop, where increased heart rate causes agitated state. In perpetuity it causes anxiety and tires the body mentally and physically.

Nerves habituate. Laying in the bath one can feel the line where the air meets the water. In a while this line disappears. Our central nervous system will start ignoring such stimuli, because after a while it looses importance to our survival. What it also means is that prolonged muscle tensions are ignored. They are not gone. In fact they are merged into the I'ness.

The neck pain which recently emerged, may be the tip of the iceberg. Underneath there is a myriad of over tense muscles which have been omitted from daily conscious attention. Attending massages may help for a while. It is easy to eliminate the (tip of the iceberg) pain. It will be back, because remaining tensions will prevent the right posture.

There are ways to eliminate complex muscle tensions. Not many have few tensions to make it an easy path. Even relaxing all of the muscles, requires a certain mental state to maintain the newly available posture. The two fold path paves the yellow brick road exiting the loop and leading to the emerald city.




----
Whoever wishes to exist as equal among equals - deserves it.




