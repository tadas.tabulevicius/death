# Ethics

Ethics is an objectively derived set of behavioral preferences to maximize happiness and minimize suffering. It is the intention heaven is paved with. It is true today, but what makes it important is the future. In the future we may have more choices. By reading Ethics you are entering into a contract, because you will fall into one of four categories depending on your choice. If you embody the non-dual perspective, you are beyond happiness and suffering - feel free to disregard Ethics. Do not pretend. Choices have consequences.

**Truth > Religion**

Beings have predispositions to truth. It makes lying stressful and demands internal justification for unethical behavior. Religion often serves as such justification. Given enough time, dogma rooted in falsehood will crumble.

**Values > Intellect**

An ethical being of lower intellect is more valuable than a highly intellectual unethical being. Intellect in an unethical being is a double edged sword. On one hand a smart sociopath can do more harm. On another - intellect along with observation and critical thinking can be used to derive a new value system. It is difficult to convince a being to update its value system if it is incapable of understanding the arguments.

Basic principles

**Non-duality**

Everything is one. Every object and experience is a temporary expression of the whole. Every being is a mere perspective. At this level ethics are arbitrary.

**Duality**

First separation arises through identification with the body. Body becomes the primary property of the perspective. For the body to survive other parts of the totality will have to be appropriated. Ethics objectively differentiate the actions to minimize suffering and maximize happiness.

Living in a rainforest it is possible to own only what can be carried. Having a cold season requires enslaving other beings and expands secondary property from what can be carried to what can be guarded.

Property implies violence as it is an exclusive right to using the thing that is owned. Starting with the body itself. It is violence for another being to try to use a body against the owner's will.

**Interactions of the perspectives**

Two beings - two perspectives identifying with two different bodies. The highest form of friendship is letting go of all property including the body in respect to the other perspective. It is entering into the non-duality contract at a smaller scale. Substantial alignment of values is a necessary prerequisite for a true friendship. The next step would be gifts and sharing. There should be no secrets. It ends with complete alignment of values while retaining personality traits.

If beings have incompatible value systems then merge is not possible. It is possible to have a mutually beneficial relationship by aligning goals or negotiating a tit for tat deal. If the deal cannot be reached then violence becomes the largest factor.

## Ethics of Violence

**Psychological violence > physical violence**

It is better to use psychological than physical violence. Threat of physical violence is a form of psychological violence. It is a warning shot. Threat is mostly backed by a reputation of using physical violence in the past. The same reputation is being staked when issuing a threat.

**Physical violence > Killing**

In general causing someone physical harm is better than killing. However there exists a point when killing becomes a more humane action than torture.

**Self defense**

It is ethical to use violence in self defense. If one is physically attacked it is ethical to use disproportionate violence in order for it to be a deterrent of violence in the future.

Preemptive attacks require extreme caution. The actions of another party should already qualify as the initiation of physical violence for the ‘counter’ attack to qualify as self defense. Therefore the threat of violence in general is not substantial grounds for the initiation of physical violence.

Likewise third party self defense should be treated with extreme caution. While it is almost always ethical to intervene in order to stop a violent altercation, it requires complete understanding of the situation for the violence level to exceed the absolute minimum required to intervene.

**Survival**

Organic bodies require organic matter to function. Under current circumstances it will most likely come at the expense of violence to other beings. Unprovoked violence against other beings to ensure one’s survival is unethical but justifiable. 

It is necessary to assign value to every being in order to determine who should be subjected to violence. The parameters determining individual’s value are ranked in such order:
1.  Value system, happiness to suffering ratio
2.  Intellect
3.  Rarity
4.  Physical size
5.  Potential lifespan
6.  Genetic proximity

**Value system**

Embodying a similar value system would rank the highest. This parameter is meaningless in low intellect beings. The value of such beings is determined by how much happiness and suffering they produce. In general carnivorous animals would be less valuable than omnivorous and omnivores would be less valuable than herbivores. Even if both animals are carnivores, it is preferable to kill the one that consumes more valuable beings.

**Longevity**

It is better to kill a turtle that can live for 30 years than the one that can live for 300. Regardless of them being the same age at the moment.

**Invested resources**

The more resources were invested into a being the more valuable it becomes. Early abortion is better than late abortion. The same is be true for a born human until it reaches its maturity. It is better to kill a baby than a 7 year old. It is better to kill a 7 year old than an 18 year old. Same rule applies to other beings. It is better to destroy an egg than to kill a chicken.

The resource argument is also applicable to physical size. If all other parameters were equal, killing a squirrel would be preferable to killing a cow. Physical size of a higher intellect being may be inversely correlated to its value since it may be indicative of a lower value system traits such as greed, selfishness.

**Rarity**

Beings of endangered species are more valuable. Killing the last remaining fish of a particular kind does more harm than killing an orca. Population recovery rate of the species is also a factor. In this regard killing an animal with high reproduction rate such as a rabbit is less harmful than killing a panda which is hard to breed.

Rarity is also the reason making intellect an important factor. Humans exhibit a wide range of IQ. To the extent that someone with lowest levels of IQ will intellectually be more similar to other higher primate species than to another human with IQ above 150. While humans with extremely high intellect could be treated as rare subspecies, their value is not likely to trump the value of other more ethical beings.

**Genetic proximity**

While genetic proximity seems arbitrary it is one of the failsafes encoded in nature. Occasional cases of cannibalism are tolerated. However once a threshold is triggered the system resets manifesting as Variant Creutzfeldt–Jakob disease. It results in a recursive wipeout of not only cannibals but carnivores in general. By extension consuming genetically distant organisms is preferable. It is better for a human to eat tuna rather than another mammal.

## Failsafes

The existence of failsafes at such a fundamental level points to intelligent design. Current human civilization’s technological sophistication could be indicating previous civilization resets. Such resets could have happened due to random calamities, but also could have occurred due to failsafes getting triggered.

Technological civilization’s reset may happen at multiple levels:
1.  Planetary - war, volcanoes, tectonic plate shifts, magnetic field shifts.
2.  Solar system - meteors, comets, solar flares
3.  Interstellar - comet-like interstellar objects, black holes

Civilization reset may be needed once technological sophistication starts to exceed the level of ethics. If it becomes a threat to already existing more advanced beings it may be in their interest to prevent the next leap. Similar to cancer being noticed once it starts to interfere with regular function of the organs. It is also easy to think of technologies that seem desirable but would be detrimental to a civilization without appropriate ethics.

It is always beneficial to move to a more ethical system. The possibility of a technological reset is just a stick factor determining how urgently it has to be done.

## Life extension

Technological advancement enables life extension. It will split humanity in many ways. An unaging human is no longer homo sapiens.

The problem of overpopulation is currently being overdramatized. Earth could easily support 30 billion or even greater population of ethical humans. However, access to technologies that stop or drastically slow down aging introduce this problem for real. If no one is dying and every human on average has at least two kids then we are dealing with an infinite population. While we are not faced with a problem of shortage of space in the cosmos, we are forced to introduce strict population control before technological advancement allows us to utilize that space. Any number below two offspring per person on average makes the potential population finite. However it is reasonable to start with a lower number and gradually increase it. One child per person policy would be a good starting point.

Happiness is a long and interesting existence. One is free to decide how much length to sacrifice in order to make the existence more interesting. And vice versa how much more boring the existence should become to make it more lengthy.

When it comes to trading one’s own happiness for the suffering of others things become more nuanced. Since a being may be overemphasizing one’s own feelings and downplaying the feelings of others it is best to take self reporting out of the equation. Otherwise someone may claim that the happiness one experienced eating a steak outweighs the suffering the cow had to endure being imprisoned and killed.

In order to maintain objectivity the ratio of happiness to suffering inflicted on the world is evaluated. This ratio and one’s attitude gives four groups of beings.

**Suffering minimalist**

Ready to sacrifice one’s happiness to reduce suffering in the world. Happiness being a composite of long and interesting existence, it would not be desirable for such a being to sacrifice the length of existence. If the person is willing to eventually consume completely synthetic food, one should have access to life extension technology without taking into account how much happiness one produces.

**Happiness maximalist**

The person concentrates on creating happiness in the world, at the same time realizing that suffering is inevitable. One may even indulge in unethical behavior that is justified by happiness outweighing the amount of created suffering. For example a lifeguard or a surgeon saving an ethical human for every cow killed and consumed as hamburgers. This position is indeed rational and overall such beings are net ethical. However the system should focus on trying to minimize the dependence on such beings. Over time their function should be automated making it perpetually harder to justify unethical behavior. Once a robot can operate as successfully as a surgeon, eating hamburgers will be increasingly more difficult to justify.

Life extension technology should be available to the happiness maximalist. Accessing the technology one enters a perpetual race of creating more happiness than suffering and should gradually migrate towards suffering minimalism over time.

**Naturalist**

Nature is relatively tolerant of unethical behavior. Naturalist is free to leverage this forgiveness while accepting the human condition. One is free to produce more suffering than happiness as long as some hard lines are not crossed. Choosing the path of a Naturalist, one is free to have as many kids as one can take care of, but forfeits any right to access life extension technology.

Prior to accessing life extension technology and having children there is no fundamental difference between Happiness maximalist and ethical Naturalist.

**Sociopath**

Does not consider ethics as long as there are no immediate consequences. Disregards the suffering of others to maximize one’s happiness. Will gladly sacrifice other beings to ensure one’s own survival. Will try to acquire access to the life extension technology to prolong one’s own life.

Prior to accessing life extension technology there is no fundamental difference between deeply unethical Naturalist and Sociopath. Violence against sociopaths is ethical in order to incapacitate or eliminate them.


Every human born today is a Naturalist. It is ideal when the choice to extend one’s natural life is done consciously. However once the technology is available there will be cases when it is applied by a 3rd party, for example by parents pre- or postnatally. It is unfortunate that one is robbed of a choice, but one is no longer a Naturalist. The same rules apply and one will fall into one of the other three categories.

**Ethical builders**

Ethics is a star which points to happiness. Not just for oneself but for others. Because it is difficult for the king to be happy in a suffering kingdom. The tendency for progress leads to desire to build a tower to heaven. Every time we try to build a tower, it is struck down. Maybe it is struck down not because we are bad engineers, but because we do not deserve it. What if the tower can ever be built by ethical builders.

----
Whoever wishes to exist as equal among equals - deserves it.