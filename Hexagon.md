# Hexagon

Hexagon is the ground floor of the tower to heaven. Not that the structure must be vertical. Just as software architecture mirrors the [organization structure](https://en.wikipedia.org/wiki/Conway's_law), so conversely by providing appropriate physical structure it is possible to facilitate healthy community dynamics. While the main ingredient remains shared [values](https://gitlab.com/tadas.tabulevicius/death/-/blob/master/Ethics.md), the hexagon is designed to enable expressing these values.

The central belief is that by placing truth as the core value, we can handle the pain which is caused by the destruction of falsehood together. Treating each other as equals and being able to fallback to Applied Ethics (AE) [voting algorithm](https://gitlab.com/tadas.tabulevicius/death/-/blob/master/Applied%20Ethics.md?ref_type=heads#how) is enough to solve the majority of problems and build a post scarcity community. It is the experiment which tests the limits of equality, honesty, and voluntarism.

**Fractal structure**

The small hexagon is built by joining 3m x 3m square walls at 120 degree angles. This makes it a 6m long and 5.2m wide room. Small hexagon serves as a private bedroom. Every being needs a peaceful place to reflect. It should easily fit a table and a wardrobe, but furnishing choice is left for the person occupying it. Ideally this is the only truly private area in the structure, achieved by using transparent walls extensively.

12 small hexagons, 3 on each side, compose a mid hexagon. There is a lounge/kitchen area in the middle. At the very center there is a fireplace, which can be seen from all sides. One dreams alone in a small hexagon and comes to the lounge to dream together. Each small hexagon has a door to the lounge area and in some cases may have a door to the outside. Heated floor is a feature of the whole mid hexagon.

Up to 9 people can comfortably live in the mid hexagon at any time. This leaves 2 small hexagons for bathrooms, 1 for entrance/storage. 1 small hexagon can be used as a garage or an office. Any unused small hexagons are allocated for any purpose by using AE voting.

In exactly the same way 12 mid hexagons, 3 on each side, compose a large hexagon. The middle is an open area with some body of water - a pond and/or fountain. Another difference is that it is possible to pass from one mid hexagon to the neighboring one directly. Large hexagon community is able to allocate mid hexagons for a particular purpose.

**Development**

Existing members have the ability to accept new members. For a community within a single large hexagon the decision to accept a new member should be unanimous. To remove a member the decision should also be unanimous by the rest of the group.

Everyone is free to leave the community at any time. Anything given to the community should be considered a donation and is not refundable at departure. To rejoin the community the same unanimous decision is needed.

Before a candidate is accepted as a member, an internship could be offered, without all member privileges. For a mature community a tiered system could be developed offering some grace period to reconsider suitability of the new members.

However equality among fully accepted members should not be sacrificed regardless of how long they have been in the community. Any decision power differences should be handled by the voting algorithm.

New mid hexagons should be developed organically. The situation where only new members are placed in a separate mid hexagon should be avoided. Some experienced members should live together and guide new members. Having multiple mid hexagons periodic migration should be encouraged.

**Decision making**

The decisions are made based on 2 principles. Primarily AE voting algorithm and secondly - the power of decision is proportional to how much one is affected by the decision. It is a good practice to have a discussion before voting on a communal decision. If the decision is unanimous there is no need to have a formal voting procedure. On the other hand, having the slightest doubt should result in voting. Everybody's trust in the voting algorithm is crucial in avoiding personal grudges. In theory anonymous voting could be possible through [zero-knowledge proofs](https://en.wikipedia.org/wiki/Zero-knowledge_proof) though the utility for small community decisions is doubtful.

Personal decisions are ruled by the second principle. As long as the decision does not affect others directly, there is no need to consult them. As an example one is free to be messy in one's private bedroom, but the lounge area should be kept in pristine condition as much as possible.

**Budget**

The goal is to have a purely voluntary community where a member contributes as much as one wants to. This will of course affect the voting power of contributors during budget allocation. A periodic contribution round is probably necessary to cover basic living costs.

A separate budget can be created for a specific purchase. The amount needed can be collected over time. It may appear rational to use nominal contribution values instead of moving averages for the single purchase budget decisions, but if it does not make a huge difference, the algorithm should be kept consistent.

Over time the budget is likely to exceed the needs of the community. This allows giving back to the outside economy by hiring people for certain jobs or implementing ethical projects.


**Concerns**

There will undoubtedly be tests along the way. Only a fraction can be identified in advance. Having kids is one of the biggest challenges. If we were able to limit the number of kids to 1 per member, then at least the sustainability question would be solved. Having A separate mid hexagon for kids with a supervising adult is also an option.

Another concern would be stale mates with bad actors. For example when a lot of people would like to remove a member but a unanimous decision cannot be reached. This could potentially end in the fracturing of the community. Unanimous decisions for removing members may be impractical for large enough communities. However such considerations should happen before the situation arises.

Community reaching the size of a large hexagon approaches [Dunbar's number](https://en.wikipedia.org/wiki/Dunbar%27s_number). Going further, homogenous culture becomes increasingly important to affirm the sense of community. In addition, fractal design beyond a large hexagon should be reconsidered even if the geographical location allows it. Possibly replacing it with the hive pattern.

**End goal**

In the future an algorithm will be developed which is able to allocate resources to maximize the overall community's happiness. Hexagon is a path which leads to developing enough trust to contribute even one's voting power to such an algorithm.


*The Second Bestagon*

*A brave new world*
*With the same old people?*
*Sure, the very second best of them.*
*Wanna build a hexagon?*

*I'll pray and cry,*
*As I always do.*
*We'll grow the hexagon,*
*I'll be my second best to You.*

----
Whoever wishes to exist as equal among equals - deserves it.
