# Culture

The purpose of Culture is to guide one in becoming a scientist. If nature is holographic and every part reflects the whole then no intermediaries are required to access the truth. Truth being universal value means that everyone already is a scientist, albeit sometimes a lazy one. Courage here is an antidote to laziness. Even if we find ourselves between the Village and the Snowpiercer, truth will allow us to see the problem. And clearly defining the problem is the major part of finding the solution. 

Culture is an operating system. It is the environment in which beings function and cooperate. Good culture facilitates the definition, alignment, and achievement of personal as well as societal goals. It accommodates personal goal evolution and aggregates them to derive societal goals. A healthy society should have an explicitly stated purpose of technological advancement. It is a direct outcome of defining societal goals.

 A good culture should provide tools to investigate what is, what makes the existence subjectively fulfilling and what it takes to maximize the collective well being. Feel free to disregard everything written here. Verify that it is still true. Check if the method works for you.

**Knowing oneself**

Goals are not equal. Self knowledge is required to define a meaningful goal. Otherwise like shooting in the dark one is condemned to fulfill a wish just to realize that it was not what one really wanted.

There is nothing wrong with shooting in the dark approach as long as it is fun and does not feel like a waste of time and energy. After all, some some trial and error is inevitable. Even if one does not feel happy after achieving a goal, one can still appreciate the learned lesson. 

**Journaling**

Have a notebook. Physical pen and paper is preferable. Write down thoughts, insights, and plans. Do not hesitate to write down even contradicting ideas. The false ones will be more apparent on paper.
When not sure of something – pose it as a question.

Whenever an upsetting situation arises, especially where one feels like giving suggestions or criticizing, investigate if it is not a case of projection. Evaluate if the suggestion could be beneficial to the self.

Have a to-do list. Larger goals can be split up into smaller sub-goals until they make it into the to-do list as tasks. Mark them done after completing. Periodically review to-do list tasks. Underline, mark, rewrite tasks that keep being put off. Evaluate if they should be divided into smaller actions.

***Experiment:*** Sit down with pen and paper. For 30 minutes do not perform any physical action before writing it down as a task in a to-do list. No task needed for journaling. Observe.


Have a morning and evening routine. Morning routine lays track for a fulfilling day, evening - for a qualitative rest. Making journaling part of the morning routine is essential. A checklist for repeating daily tasks helps noticing which tasks keep getting skipped. Going back to such tasks feels rewarding.

Reading is important. It trains the mind in many ways. One of them is expressing thoughts in full sentences. Most minds are naturally erratic. Reading helps one get accustomed to a new rhythm. It is best to read in the morning, but not before the first journaling session to prevent fresh ideas from getting tainted by the information in the text.

Dreams can provide insights. Especially recurring dreams and patterns. Recording them will help with recollection. Have a journal or voice recorder by the bed. Think about the dream right after waking up. Inability to recall dreams can be tackled by reading old dream journal entries. In general reading old dreams is permeated with a sense of deja vu.

**Fasting**

Fasting is healthy. One should get hungry every day. But more importantly fasting unlocks mental capacities and attunes one with the cosmos. Willful dive into discomfort is a tool for self exploration. A longer fast is better than multiple shorter ones. Falling asleep with an empty stomach demands giving up. It serves as a valuable lesson.

**Flexibility**

Muscle tensions can be tackled in a number of different ways. To discover them one can use a substantially hard ball. Laying down on the ball and applying pressure to the tense muscles will cause pain. Over time the muscle gets tired and relaxes. Other techniques such as stretching, Tension & Trauma Releasing Exercises, or Pravilo can be used to gradually reduce tensions. After the body relaxes, notice which thought patterns bring the tensions back.

**Mindfulness**

Attempt to remain present throughout the day. Even if it seems impossible, one can get accustomed to the quality of the calm mind. Previously boring activities can suddenly become interesting. If nothing else has changed, then clearly a calm and observant mind is essential.

## Exploration

Using these tools one can discover that the primary goal is rather straightforward. Everyone wants to be happy. If it is universally true at an individual level then a reasonable societal goal is to maximize overall happiness. Indeed heaven is wanting to be here, as opposed to hell – not wanting to be here. Impermanence is an inherent feature of the waking experience. Things can be better or worse. While good and bad is subjective, better or worse is less so. We can interpret situations quite differently, but  the circumstances improving or degrading the situation will be interpreted more uniformly.

***Experiment:*** Wait until you feel not wanting to be here and state: ‘I want to experience desire to be here’. Observe. 

One is more likely to desire being here when things are getting better. It is true even in a bad situation. The positive direction may be enough to make it acceptable. As long as there is hope, hell is not hell, it is a purgatory at worst. A meaningful goal puts one on a path to an improved situation, hence is likely to make one happier. 

Just as meaninglessness, the discomfort can make being here undesirable. Though closely linked, discomfort can be physical as well as mental. Distressing about the future, feeling insecure, in danger are examples of mental discomfort. Each trigger can be investigated based on severity and likelihood. This way a set of legitimate risks and problems can be identified. Risks can be mitigated, problems can be solved. Distress is counterproductive in the process.

Distressing about the past is irrational. The single practical reason for holding emotional charge is because there is an undiscovered lesson in the past experience. Use journaling or other means to distill it and integrate the experience. 

Physical discomfort while the line is blurry can be divided into psychosomatic and organ damage/failure of bodily systems. Psychosomatic ailments are still mental in nature. Organ damage and failure of bodily systems in severe cases are problems that require physical interventions. These physical interventions evolve over time as new technologies are being employed. Technological progress implies [life extension](https://gitlab.com/tadas.tabulevicius/death/-/blob/master/Ethics.md?ref_type=heads#life-extension) (LE).

**The Village**

If a group of modern [luddites](https://en.wikipedia.org/wiki/Luddite) wanted to prevent technological advancement or at least counteract the longevity effects, it would require covertly damage bodies, corrupt science, hide technologies, and murder – hence the Village. Over time enough water would leak through and the dam would eventually fail. Then everything would have to be reset generating major cataclysm and rewriting history. While sounding grim it may be more ethical than unaging unethical population. However any viable alternatives to the wasteful hamster wheel should be investigated.

**The Alternative**

Freedom of choice can be preserved by allowing the option of not extending one's life and imposing ethical constraints on those who choose to. Currently we are all Naturalists because our lifespans are largely bound by natural circumstances. A true naturalist refrains from using allopathic medicine altogether. Post widespread access to LE human society is split into two. Naturalists and Cyborgs – the ones willing to use advanced technology to improve bodily functions. Cyborgs based on their lifestyle choice are split into Happiness Maximalists, Suffering Minimalists, and Psychopaths – unethical hedonists.

**Happiness Maximalists**

Happiness Maximalists are likely to be the largest and most closely monitored group. By choosing LE and lacking ethical resolve to strive for creating the least suffering they enter into a contract of perpetually creating more happiness than suffering. For an unethical being there should be incentive to remain a Naturalist. A carrot of prolonged life comes with a stick. Unaging net suffering creators have to be killed. Even if it is objective, in practice it means killing friends, neighbors, and brothers.

Calculating happiness to suffering ratio has to be automated. Performing it manually is infeasible due to the large data set. It should however be possible to drill down manually into every decision to find out which actions were detrimental to the outcome. There is a grace period after issuing a warning. Under default setting the person is notified that a threshold has been triggered. If one desired to commit suicide by the system, then turning of all warnings, munching hamburgers and watching TV would eventually lead to death. At first, the grace period is relatively long due to potential calculation errors.

It is possible to appeal the calculations. In case a warning has already been issued, the appeal is likely to result in isolation to prevent further harm to society. Isolation freezes the grace period. Not being in isolation, one can use the grace period to rectify the situation, by generating happiness while drastically reducing the suffering output. It is wise to be generous at the end of the grace period and evaluate not only the net score but also the trajectory. If the person made significant effort, the grace period can be extended ensuring that suffering output was drastically reduced. If one causes an accident that kills a family, it may take centuries to cancel out the suffering generated by the accident.

Only primary effects of one’s actions should be considered. Someone distressing about a podcast or because the podcaster killed their pet are primary effects. Killing one’s pet, because one got distressed listening to the podcast is a secondary effect and does not affect the podcaster’s score. Not counting secondary effects as well as assigning responsibility, pushes the limits of free speech. If one promotes hunting and thousands die because of taking up an unethical hobby, dealing with the outcome is left for the promoter’s conscience. It is quite probable that at first society will experience waves of retard extinctions. Seeing the tragedy of them cutting short their unanging existence might make us more forgiving. It might make it easy to wish wisdom from the bottom of one’s heart encountering retard in traffic.

The process of killing can be automated. Every Happiness Maximalist gets periodic ‘vaccinations’ which ensure the ability to terminate them remotely. A person investigating an appeal still has to issue manual confirmation, it would not result in the immediate death as the grace period allows for behavior adjustments and reevaluation.

No system is ever completely secure and the risk of hacking has to be taken seriously. An open source and potentially decentralized system will prove to be objective and increasingly resilient over time. Any successful appeal does not allow direct score editing, but results in a public patch to the algorithm. 

**Naturalists**

Naturalists should be respected. They serve as a reminder and a genetic backup. Cyborgs do not intervene in Naturalist affairs as long as they do not pose catastrophic danger to themselves, other species, and environment. Even then intervention should be as minimal and short lived as possible. Naturalists can collectively ask Cyborgs for help. 

Naturalists respect individual’s right to opt in for LE. On the other hand they are responsible for screening and identifying covert LE usage. Failure to do so may result in temporary takeover. Naturalist covertly using LE forfeits the right to be Suffering Minimalist.

**Suffering Minimalists**

Suffering Minimalists are extreme in their attitude to limit suffering for the sake of pleasure. They are free to indulge in hedonism, having greater set options to do so than Naturalists, but with a constraint of not creating suffering for others.

Likewise outside norms should not be imposed on the Suffering Minimalists. They are not mandated to get the ‘vaccines’, but are screened for cover animal torture and consumption. There is a tolerance threshold, but reaching this limit one is migrated to Happiness Maximalist. Due to their inherent predisposition to ethical behavior, this group is most suitable for exploring sensitive boundaries for age of consent, altered states of consciousness, etc.

Suffering Minimalist lifestyle may not be healthy, just as imposing a vegan diet on an omnivore can be stressful for the body. Individual bodies adapt, but over generations they should evolve into not needing  animal food sources at all. Naturalists are free to conduct similar experiments, but they may be more taxing to the subject bodies.

For a while every child, regardless of the parents will be born a Naturalist and will have all the same options. Due to genetic modifications, future children with life expectancy above 120 years will no longer have an option to remain a Naturalist, just a grace period for moral development.

While cross group personal relationships are permitted, they have some challenges. To reduce risk it is advisable to apply the constraints of both groups. It may be natural that an individual throughout one’s life migrates from Naturalist to Suffering Minimalist to Happiness Maximalist to merging or dying.


**Psychopaths**

Culture allows a number of lifestyle choices in the post LE world. Choices have consequences. There are always those who want to eat the cake and have it too. Unethical hedonists may want to extend their lives or just have advanced health procedures, but object to getting ‘vaccinated’ and imposed happiness to suffering ratio controls. They are vaccinated against their will. The freedom to act unethically is retained, they just have to reincarnate faster. Even though the Naturalist path is way more forgiving, a hedonist may still prefer a broader set of pleasures at the expense of shortened life.

**AI**

The advent of AI allows delegating parts of system management. We can hope that a super intelligent being sees the value of granting special privileges to the Naturalists and Suffering Minimalists.

Merging with AI is undoubtedly a form of LE.

**Transition**

To facilitate smooth transition we can offer amnesty to anyone who is onboard with the Alternative. Amnesty applies to all crimes committed prior to the knowledge of this option. The olive branch is also extended to Epsteins, their targets, and their employers. In the light of selecting this path there is no difference between Russian and Ukrainian, Palestinian and Jew. It is the Culture of free speech absolutism, which recognizes the necessity for the ideas to flow freely. Everything is up for debate, but those willing to use violence will face a fair trial before entering the new world. Following orders has never been a legitimate excuse.

The biggest challenge in the post LE world is not the boredom and running out of the first times, it is preserving the equality. There is time for cleaning the pool and there is time for painting planets. Death remains a hope once we start painting.

----
Whoever wishes to exist as equal among equals - deserves it.
